#!/usr/bin/env python3

import sys
import socket
import string
import os
import time
import math

FILE = "timer.txt"

os.system("cls")
days = ""
hours = ""
minutes = ""
seconds = ""

f = open(FILE)
lines = f.readlines()
f.close()

days = float(lines[0])

hours = float(lines[1])

minutes = float(lines[2])

seconds = float(lines[3])

while True:


	print(str(math.trunc(days)) + " days " + str(math.trunc(hours)) + " hours" + str(math.trunc(minutes)) + " minutes " + str(math.trunc(seconds)) + " seconds")

	if seconds == 60:
		if minutes == 60:
			if hours == 24:
				days = days+1
			else:
				minutes = minutes+1
		else:
			seconds = seconds+1

	lines = str(math.trunc(days)) + "\n",str(math.trunc(hours)) + "\n",str(math.trunc(minutes)) + "\n",str(math.trunc(seconds)) + "\n"
	with open(FILE, "w") as f:
		f.writelines(lines)

	time.sleep(1)


