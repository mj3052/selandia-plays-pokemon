#!/usr/bin/env python3

import sys
import socket
import string
import os
import time

# The host and port of the IRC chat, does not need to be changed
HOST = "irc.twitch.tv"
PORT = 6667

# Auth key for verifying with IRC
AUTH = "oauth:8uhg5tbrzlnzhnv259qqzwa7144ryaw" #Obtained at http://www.twitchapps.com/tmi 

# These should in most cases be your Twitch username
NICK = "selandiaplayspokemon"
IDENT = "selandiaplayspokemon"
REALNAME = "selandiaplayspokemon"
MASTER = "selandiaplayspokemon"
CHAT_CHANNEL = "selandiaplayspokemon" #Typically username of the streamer

FILE = "commands.txt"

# Creating variables for use in all scopes
readbuffer = ""
out = ""
x = 0

# Set codepage to 65001 (same as the command-lines when running)
os.system("chcp 65001")

# Create a socket and connect to IRC
s=socket.socket( )
s.connect((HOST, PORT))

# Send the commands needed to connect to the correct IRC chat
s.send(bytes("PASS %s\r\n" % AUTH, "UTF-8"))
s.send(bytes("NICK %s\r\n" % NICK, "UTF-8"))
s.send(bytes("USER %s %s bla :%s\r\n" % (IDENT, HOST, REALNAME), "UTF-8"))
s.send(bytes("JOIN #%s\r\n" % CHAT_CHANNEL, "UTF-8"));
s.send(bytes("PRIVMSG #%s :Connected\r\n" % CHAT_CHANNEL, "UTF-8"))

# The infinite loop checking for receiving chat messages and parsing them
while 1:
    # Read the buffer and create an array by splitting at newlines
    readbuffer = readbuffer+s.recv(1024).decode("UTF-8", errors="ignore")
    temp = str.split(readbuffer, "\n")
    readbuffer=temp.pop( )

    # For every message (line) received
    for line in temp:
        x = 0
        out = ""
        line = str.rstrip(line)
        line = str.split(line)

        for index, i in enumerate(line):
            # If ping (or something like that) has been received, send PONG
            if(line[index] == "ING: " or line[index] == "ING:" or line[index] == "ING" or line[index] == "PING:" or line[index] == "PING: " or line[index] == "PING"):
                s.send(bytes("PONG tmi.twitch.tv\r\n", "UTF-8"))
            if x == 0:
                user = line[index]
                user = user[1:].split('!')[0] + ": "
            if x == 3:
                out += line[index]
                out = out[1:]
            if x >= 4:
                out += " " + line[index]
            x = x + 1
        print(user + out)

        # Determine if a command has been entered, if it has write it and the user to commands.txt
        if out.lower() == 'up':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'up')
        if out.lower() == 'right':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'right')
        if out.lower() == 'down':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'down')
        if out.lower() == 'left':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'left')
        if out.lower() == 'a':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'a')
        if out.lower() == 'b':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'b')
        if out.lower() == 'start':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'start')
        if out.lower() == 'select':
            with open(FILE, "a") as f:
                f.write('\n' + user + 'select')
