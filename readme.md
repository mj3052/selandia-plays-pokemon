# Selandia plays Pokémon

## What is this?
Selandia plays Pokémon is a recreation of "TwitchPlaysPokémon". It basically does the same, it is just public for everyone. It is roughly based on another project found on Github, but it has been changed fundamentally, and is therefore a creation of its own.

## How do I run it?
Well, to run it you will need one thing:    
- A machine running Windows

### 1. Installing requirements
Before running anything at all you will need to install the following:    
- Python 3.3.4 ([32-bit][1], [64-bit][2])    
- The Python modules used ([32-bit][3], [64-bit][4])

### 2. Configuring
When these are installed you just need to configure irc.py with your settings (strings in top of the file).

When you have entered your settings place your ROM in the folder and change the ROM name in top of GameController.py. Then set Visual Boy Advance (included in the project) as default program for ROM files (GBA, GBC).

### 3. Running
Open up two command-prompts and type: `chcp 65001` in both.

Browse to the folder containing the project and run: `python irc.py` in the first window. In the second window run: `python GameController.py`

The game should now start receiving the commands sent on your channel IRC.

## License
Copyright (c) 2014 Mathias Jensen
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

  [1]: http://www.python.org/ftp/python/3.3.4/python-3.3.4.msi
  [2]: http://www.python.org/ftp/python/3.3.4/python-3.3.4.amd64.msi
  [3]: http://sourceforge.net/projects/pywin32/files/pywin32/Build%20218/pywin32-218.win32-py3.3.exe/download
  [4]: http://sourceforge.net/projects/pywin32/files/pywin32/Build%20218/pywin32-218.win-amd64-py3.3.exe/download